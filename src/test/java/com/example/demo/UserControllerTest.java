package com.example.demo;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


public class UserControllerTest extends AbstractTest{
	@Override
	   @Before
	   public void setUp() {
	      super.setUp();
	   }
	@Test
	public void getAllUsers() throws Exception{
		String uri = "/users/";
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
	         .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
	      
	      int status = mvcResult.getResponse().getStatus();
	      assertEquals(200, status);
	      String content = mvcResult.getResponse().getContentAsString();
	      
	      JSONArray jsonArray = new JSONArray(content);
	      ArrayList<String> stringArray = new ArrayList<String>();
	      for (int i = 0; i < jsonArray.length(); i++) {
	          stringArray.add(jsonArray.getString(i));
	      }
	      
	      assertTrue(stringArray.size()> 0);
	}
		
  @Test
  public void getUsersById() throws Exception {
     String uri = "/users/show/5f8694834fd4510d03053946";
     MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
        .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
     
     int status = mvcResult.getResponse().getStatus();
     assertEquals(200, status);
     String content = mvcResult.getResponse().getContentAsString();
     JSONObject jsonObject = new JSONObject(content);
     assertEquals("Joe",jsonObject.getString("userName"));
     assertEquals("joe4",jsonObject.getString("userId"));
     assertEquals("joe@gmail.com",jsonObject.getString("emailId"));
     assertEquals("userjoe",jsonObject.getString("password"));
     
  } 
  
	@Test
	public void testLoginForRegisteredUser() throws Exception{
		String uri = "/users/login/joe@gmail.com/userjoe";
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
	         .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
	      
	      int status = mvcResult.getResponse().getStatus();
	      assertEquals(200, status);
	      String content = mvcResult.getResponse().getContentAsString();
	      assertEquals("true",content);
	}
	
	@Test
	public void testLoginForNotRegisteredUser() throws Exception{
		String uri = "/users/login/abc@gmail.com/user88";
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
	         .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
	      
	      int status = mvcResult.getResponse().getStatus();
	      assertEquals(200, status);
	      String content = mvcResult.getResponse().getContentAsString();
	      assertEquals("false",content);
	}
	
	@Test
	   public void testRegisterForNewUser() throws Exception {
	      String uri = "/users/register";

	      
	      JSONObject jsonObject = new JSONObject();
	      
	      jsonObject.put("userName", "Joe");
	      jsonObject.put("userId", "joe4");
	      jsonObject.put("emailId", "joe@gmail.com");
	      jsonObject.put("password", "userjoe");
	      
	      String inputJson = jsonObject.toString();
	      
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
	         .contentType(MediaType.APPLICATION_JSON_VALUE)
	         .content(inputJson)).andReturn();
	      String uname=(String) jsonObject.get("userName");
	      String uid=(String) jsonObject.get("userId");
	      String uemail=(String) jsonObject.get("emailId");
	      String upassword=(String) jsonObject.get("password");
	      int status = mvcResult.getResponse().getStatus();
	      assertEquals(200, status);
	      assertEquals("Joe", uname);
	      assertEquals("joe4", uid);
	      assertEquals("joe@gmail.com", uemail);
	      assertEquals("userjoe", upassword);
	      
	   }
  
	@Test
	   public void testRegisterForRegisteredUser() throws Exception {
	      String uri = "/users/register";

	      
	      JSONObject jsonObject = new JSONObject();
	      
	      jsonObject.put("userName", "Joe");
	      jsonObject.put("userId", "joe4");
	      jsonObject.put("emailId", "joe@gmail.com");
	      jsonObject.put("password", "userjoe");
	      
	      String inputJson = jsonObject.toString();
	      
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
	         .contentType(MediaType.APPLICATION_JSON_VALUE)
	         .content(inputJson)).andReturn();
	      
	      int status = mvcResult.getResponse().getStatus();
	      assertEquals(200, status);
	      String content = mvcResult.getResponse().getContentAsString();
	      assertEquals("false",content);
	      
	   }

  
  
//@Test
//public void deleteUsers() throws Exception {
//   String uri = "/users/5f8538a538d3de4833187a78";
//   MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
//   int status = mvcResult.getResponse().getStatus();
//   assertEquals(200, status);
//   
//}
	
}
