package com.example.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.example.demo.model.Trade;
import com.example.demo.model.TradeState;
import com.example.demo.model.TradeType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
//import com.google.gson.Gson;
//import com.google.gson.JsonObject;

public class TradeControllerTest extends AbstractTest {
   @Override
   @Before
   public void setUp() {
      super.setUp();
   }
   @Test
   public void getTradeList() throws Exception {
      String uri = "/trade/";
      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
         .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
      
      int status = mvcResult.getResponse().getStatus();
      assertEquals(200, status);
      String content = mvcResult.getResponse().getContentAsString();
      
      JSONArray jsonArray = new JSONArray(content);
      ArrayList<String> stringArray = new ArrayList<String>();
      for (int i = 0; i < jsonArray.length(); i++) {
          stringArray.add(jsonArray.getString(i));
      }
      
      assertTrue(stringArray.size()> 0);
   }
   
   @Test
   public void getTradeById() throws Exception {
	   String uri = "/trade/5f8838087a89a93d267f7b55";
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
	         .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
	      
	      int status = mvcResult.getResponse().getStatus();
	      assertEquals(200, status);
	      String content = mvcResult.getResponse().getContentAsString();
	      JSONObject jsonObject = new JSONObject(content);
      
      assertEquals("ajeetha",jsonObject.getString("userId"));
      assertEquals("BUY",jsonObject.getString("type"));
      assertEquals("IBM",jsonObject.getString("ticker"));
      assertEquals(200,jsonObject.getInt("quantity"));
      assertEquals("20.0",jsonObject.getString("price"));
      
   }
   
   
//   @Test
//   public void createTrade() throws Exception {
//      String uri = "/trade/";
//
//      
//      JSONObject jsonObject = new JSONObject();
//      
//      jsonObject.put("status", "CREATED");
//      jsonObject.put("type", "BUY");
//      jsonObject.put("ticker", "IBM");
//      jsonObject.put("quantity", 100);
//      jsonObject.put("price", 100);
//      jsonObject.put("userId", "gayathiri");      
//      String inputJson = jsonObject.toString();
//      
//      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
//         .contentType(MediaType.APPLICATION_JSON_VALUE)
//         .content(inputJson)).andReturn();
//      
//      int status = mvcResult.getResponse().getStatus();
//      assertEquals(200, status);
//      
//   }
//
// @Test
// public void updateCREATEDTrade1() throws Exception {
//    String uri = "/trade/5f85dbb8417bb03dc6b64383";
//    JSONObject jsonObject = new JSONObject();
//    jsonObject.put("quantity",100); 
//    jsonObject.put("userId","Ruby");
//    jsonObject.put("ticker","FB");
//    String inputJson = jsonObject.toString();
//    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri)
//            .contentType(MediaType.APPLICATION_JSON_VALUE)
//            .content(inputJson)).andReturn();
//    int status = mvcResult.getResponse().getStatus();
//    assertEquals(200, status);
//    String content = mvcResult.getResponse().getContentAsString();
//    assertEquals("Update Successful",content);
//    
// }
// @Test
// public void updateCREATEDTrade2() throws Exception {
//    String uri = "/trade/5f87dd3cde9bed4f95842a3c";
//    JSONObject jsonObject = new JSONObject();
//    jsonObject.put("quantity",100); 
//    jsonObject.put("userId","Ruby");
//    jsonObject.put("ticker","FB");
//    jsonObject.put("state","FILLED");
//    String inputJson = jsonObject.toString();
//    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri)
//            .contentType(MediaType.APPLICATION_JSON_VALUE)
//            .content(inputJson)).andReturn();
//    int status = mvcResult.getResponse().getStatus();
//    assertEquals(200, status);
//    String content = mvcResult.getResponse().getContentAsString();
//    assertEquals("Can only modify Created trades",content);
//    
// }

//   @Test
//   public void deleteProduct() throws Exception {
//      String uri = "/trade/5f85dbb8417bb03dc6b64383";
//      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
//      int status = mvcResult.getResponse().getStatus();
//      assertEquals(200, status);
//      
//   }
}

