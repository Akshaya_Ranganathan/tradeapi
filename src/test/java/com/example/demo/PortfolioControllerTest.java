package com.example.demo;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.json.JSONArray;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

public class PortfolioControllerTest extends AbstractTest{
	@Override
	   @Before
	   public void setUp() {
	      super.setUp();
	   }
	@Test
	public void testPortfolioForExistingUserId()throws Exception{
		String uri = "/UserPortfolio/ajeetha";
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
	         .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
	      
	      int status = mvcResult.getResponse().getStatus();
	      assertEquals(200, status);
	      String content = mvcResult.getResponse().getContentAsString();
	      
	      JSONArray jsonArray = new JSONArray(content);
	      ArrayList<String> stringArray = new ArrayList<String>();
	      for (int i = 0; i < jsonArray.length(); i++) {
	          stringArray.add(jsonArray.getString(i));
	      }
	      
	      assertTrue(stringArray.size()> 0);
	}
	@Test
	public void testPortfolioForNotExistingUserId()throws Exception{
		String uri = "/UserPortfolio/akshay";
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
	         .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
	      
	      int status = mvcResult.getResponse().getStatus();
	      assertEquals(200, status);
	      String content = mvcResult.getResponse().getContentAsString();
	      assertEquals("[]",content);
	}

}
