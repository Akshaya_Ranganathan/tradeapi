package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.model.Users;
import com.example.demo.repository.UserRepository;

@RestController
@RequestMapping("/users")
public class UserController {
	
	@Autowired
	private UserRepository repository;
	
	  
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Users> getAllUsers() {
	  return repository.findAll();
	}
     
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public Boolean createUser(@Valid @RequestBody Users user  ) {
		int flag = 0;
		List <Users> user1 = new ArrayList<Users>();
	  	  user1 = repository.findAll();
	  	  
	  	  for(Users u: user1) {
	  		  
	  		  if(u.getUserId().equals(user.getUserId()) ) {
	  			  System.out.println("user id alredy exists");
	  			  flag = 1;
	  		  }
	  	  }
	  	  if(flag == 0)
	  	  {
	  		user.set_id(ObjectId.get());
	  		repository.save(user);
	  		return true;
	  	  }
	  	  else
	  	  {
	  		  return false;
	  	  }
	}
 

	@RequestMapping(value = "/login/{emailId}/{password}", method = RequestMethod.GET)
	public Boolean loginUser(@PathVariable("emailId") String emailId, @PathVariable("password") String password,
			HttpSession session) {
	  List <Users> user = new ArrayList<Users>();
	  user = repository.findAll();
	  
	  for(Users u: user) {
		  
		  if(u.getEmailId().equals(emailId) && u.getPassword().equals(password)) {
			  session.setAttribute("username", u.getUserId());
			  return true;
		  }
	  }
	  return false;
	}
    
	@RequestMapping(value = "/show/{id}", method = RequestMethod.GET)
	public Users show(@PathVariable("id") String id) { 
      List <Users> user = new ArrayList<Users>();
  	  user = repository.findAll();
  	  
  	  for(Users u: user) {
  		  
  		  if(u.get_id().equals(id) ) {
  			  return u;
  		  }
  	  }
  	  return null;
  	}
		 


	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public void modifyPetById(@PathVariable("id") ObjectId id,@Valid @RequestBody Users userCreated) {
	 
	  userCreated.set_id(id);
	  repository.save(userCreated);
	  
	}
	@RequestMapping(value = "/signin", method = RequestMethod.GET)

	@ResponseBody
	public ModelAndView  signin() { 
		
		ModelAndView modelAndView = new ModelAndView(); 
		modelAndView.setViewName("sign-in"); 
		return modelAndView; 
		}
		
	@GetMapping(value = "/login")
	public String getLogin(HttpSession session) {
		if(session.getAttribute("username")!= null) {
			return (String)session.getAttribute("username");
		}
		else {
			return "not logged in";
		}
	}

	
	
	

}