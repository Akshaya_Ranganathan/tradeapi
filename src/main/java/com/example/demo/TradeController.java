package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;



import com.example.demo.model.Trade;
import com.example.demo.repository.TradeRepository;

@RestController
@RequestMapping("/trade")
public class TradeController {
	@Autowired
	private TradeRepository repository;

	Logger logger = LoggerFactory.getLogger(TradeController.class);

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Trade> getAllTrades() {
	logger.info("User viewed all the trades from repository");
	System.out.println("test1");
	

	return repository.findAll();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Trade getTradeById(@PathVariable("id") ObjectId id) {
		//logger.info("User viewed this trade which has the tradeId {} " id);
		logger.info("User viewed a particular trade" );
	return repository.findBy_id(id);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public String modifyTradeById(@PathVariable("id") ObjectId id, @Valid @RequestBody Trade trade) {
		
	trade.set_id(id);
	if(trade.getState().toString().equals("CREATED")||trade.getState().toString().equals("FILLED")) {
		System.out.println("test2");
		repository.save(trade);
		logger.info("User updated the trade status from CREATED to CANCELLED" );
		return "Update Successful";
		
	}
	else
		logger.info( "User tried to update trade status but was denied access" );
		return "Can only modify Created trades";
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Trade createTrade(@Valid @RequestBody Trade trade) {
	trade.set_id(ObjectId.get());
	logger.info("User posted a trade");
	//logger.info("User posted a trade which has the tradeId {} " id);


	repository.save(trade);
	return trade;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteTrade(@PathVariable ObjectId id) {
		logger.info( "User deleted a trade" );
		//logger.info("User deleted a trade which has the tradeId {} " id);
	repository.delete(repository.findBy_id(id));
	}
	
	@RequestMapping(value = "/home1", method = RequestMethod.GET)
	
	@ResponseBody
	public ModelAndView home1() { 
		ModelAndView modelAndView = new ModelAndView(); 
		modelAndView.setViewName("home"); 
		return modelAndView; 
		}
	
	
    @RequestMapping(value = "/crud", method = RequestMethod.GET)
	
	@ResponseBody
	public ModelAndView test() { 
    	
    	ModelAndView modelAndView = new ModelAndView(); 
		modelAndView.setViewName("tradeList"); 
		return modelAndView; 
		}
@RequestMapping(value = "/home", method = RequestMethod.GET)
	
	@ResponseBody
	public ModelAndView  home() { 
    	
    	ModelAndView modelAndView = new ModelAndView(); 
		modelAndView.setViewName("landing"); 
		return modelAndView; 
		}
@RequestMapping(value = "/main", method = RequestMethod.GET)

@ResponseBody
public ModelAndView  main() { 
	ModelAndView modelAndView = new ModelAndView(); 
	modelAndView.setViewName("trade_crud"); 
	return modelAndView; 
	}
	


@RequestMapping(value = "/advice", method = RequestMethod.GET)

@ResponseBody
public ModelAndView  advice() { 
	ModelAndView modelAndView = new ModelAndView(); 
	modelAndView.setViewName("advice"); 
	return modelAndView; 
	}
	
}
