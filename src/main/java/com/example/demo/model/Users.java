package com.example.demo.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class Users {
	@Id
	public ObjectId _id;
	
	public String userName;
	public String userId;
	public String emailId;
	public String password;
	
	public Users(ObjectId _id, String userName, String userId, String emailId, String password) {
		super();
		this._id = _id;
		this.userName = userName;
		this.userId = userId;
		this.emailId = emailId;
		this.password = password;
	}

	public Users() {
		// TODO Auto-generated constructor stub
	}

	public String get_id() {
		return _id.toHexString();
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}


	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Users [_id=" + _id + ", userName=" + userName + ", userId=" + userId + ", emailId=" + emailId
				+ ", password=" + password + "]";
	}
	
	
	
	
	
}
