package com.example.demo.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.demo.model.Users;


public interface UserRepository extends MongoRepository<Users, String> {
	Users findBy_id(ObjectId _id);
	Boolean existsBy_id(ObjectId _id);
}
