package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.model.Trade;
import com.example.demo.model.Users;
import com.example.demo.repository.TradeRepository;

@RestController
@RequestMapping("/UserPortfolio")
public class PortfolioController {
	
	@Autowired
	private TradeRepository repository;
	
	@RequestMapping(value = "/{userid}", method = RequestMethod.GET)
	public List<Trade> getTradeById(@PathVariable("userid") String userid) {
		List <Trade> tradeList = new ArrayList<Trade>();
		tradeList = repository.findAll();
		List <Trade> userTrade = new ArrayList<Trade>();
	  	  for(Trade t: tradeList) {
	  		  
	  		  if(t.getUserId().equals(userid) ) {
	  			  userTrade.add(t);
	  		  }
	  	  }
	  	  return userTrade;
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView  main() { 
		
		ModelAndView modelAndView = new ModelAndView(); 
		modelAndView.setViewName("portfolio"); 
		return modelAndView; 
		}

}
